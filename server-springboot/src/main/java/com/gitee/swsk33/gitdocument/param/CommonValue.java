package com.gitee.swsk33.gitdocument.param;

/**
 * 存放常用的常量值
 */
public class CommonValue {

	/**
	 * 运行程序的用户名
	 */
	public static final String RUN_USER_NAME = System.getProperty("user.name");

	/**
	 * Git仓库头指针名称
	 */
	public static final String GIT_HEAD_POINTER = "HEAD";

}