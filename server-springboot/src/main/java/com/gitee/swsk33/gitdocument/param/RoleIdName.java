package com.gitee.swsk33.gitdocument.param;

/**
 * 存放角色id和名的常量类
 */
public class RoleIdName {

	/**
	 * 预留管理员(1)
	 */
	public static final String PRESERVE_ADMIN_NAME = "ROLE_PRESERVE_ADMIN";

	/**
	 * 预留管理员的id
	 */
	public static final int PRESERVE_ADMIN_ID = 1;

	/**
	 * 管理员(2)
	 */
	public static final String ADMIN_NAME = "ROLE_ADMIN";

	/**
	 * 管理员的id
	 */
	public static final int ADMIN_ID = 2;

	/**
	 * 普通成员(3)
	 */
	public static final String MEMBER_NAME = "ROLE_MEMBER";

	/**
	 * 普通成员的id
	 */
	public static final int MEMBER_ID = 3;

	/**
	 * 写作者(4)
	 */
	public static final String WRITER_NAME = "ROLE_WRITER";

	/**
	 * 写作者的id
	 */
	public static final int WRITER_ID = 4;

}